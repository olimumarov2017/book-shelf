export const MARK_TEXT_REG = /\*\*(.+?)\*\*/g;
export const EMAIL_REG = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
