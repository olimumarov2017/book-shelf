import axios from 'axios';
import { getToken } from '../lib/localeStorage';

const baseURL = 'https://gutendex.com'
const secret = '2892678138d8d793a28fc49055095d8b'
const token = 'apikey 0M8tJvP2yv37OAoSVqivsL:5WcJWZGDKETxiTtnmSJkJa'

export const $api = axios.create({
  baseURL: baseURL,
  // withCredentials: true,
});

$api.interceptors.request.use((config) => {
  const key = getToken('key');

  if (config.headers && key.token && baseURL) {
    // config.headers.Key = key.token;
    // config.headers.Sign = secret;
    // config.headers.authorization = `${token}`;
    config.headers['Content-Type'] = 'application/json';
  }
  return config;
});
