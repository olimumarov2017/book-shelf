type Token = {
    token: string | null,
    status: 'authenticated' | 'no-authenticated' 
}

export const getToken = (key: string): Token => {
    try {
        const data = localStorage.getItem(key)
        return {
            token: data,
            status: data ? 'authenticated' : 'no-authenticated'
        }
    } catch (error) {
        console.log(error)
        return {
            token: null,
            status: 'no-authenticated'
        };
    }
}

export const setToken = (key: string, value: string) => {
    try {
        localStorage.setItem(key, value)
    } catch (error) {
        console.log(error)
    }
}


export const removeToken = (key: string) => {
    try {
        localStorage.removeItem(key)
    } catch (error) {
        console.log(error)
    }
}