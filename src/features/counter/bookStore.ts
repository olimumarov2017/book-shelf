import { IBookData } from '@/sections/Books/model/types/book'
import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'

export interface CounterState {
  isUserAuth: boolean,
  isLoading: boolean,
  userShelfData: IBookData[] | null,
  notification: {
    status: 'success' | 'error',
    text: string,
  } | null
}

const initialState: CounterState = {
  notification: null,
  isLoading: false,
  isUserAuth: false,
  userShelfData: null,
}

export const bookStore = createSlice({
  name: 'bookStore',
  initialState,
  reducers: {
    setIsUserAuth: (state) => {
      state.isUserAuth = !state.isUserAuth
    },

    setIsLoading: (state, action: PayloadAction<boolean>) => {
      state.isLoading = action.payload
    },

    setNotification: (state, action: PayloadAction<{ status: 'success' | 'error', text: string }>) => {
      state.notification = action.payload
    },

    setUserShelfData: (state, action: PayloadAction<{ data: IBookData, action: 'added' | 'deleted' }>) => {
      const { data, action: operation } = action.payload;

      state.userShelfData = !state.userShelfData ? [data] :
        operation === 'added' && state.userShelfData.findIndex(item => item.id === data.id) === -1 ?
          [...state.userShelfData, data] :
          operation === 'deleted' ? state.userShelfData.filter(item => item.id !== data.id) : state.userShelfData;
    }

  },
})

// Action creators are generated for each case reducer function
export const {
  setIsUserAuth,
  setNotification,
  setIsLoading,
  setUserShelfData
} = bookStore.actions

export default bookStore.reducer