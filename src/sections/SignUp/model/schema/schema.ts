import * as yup from 'yup';

export const schema = yup.object({
  name: yup.string().required('Name is required'),
  email: yup.string().email('Invalid email').required('Email is required'),
  key: yup.string().required('Key is required'),
  secret: yup.string().required('Secret is required'),
});