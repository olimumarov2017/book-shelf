export interface ISignUp {
    name: string,
    email: string,
    key: string,
    secret: string
}

export interface IUserData {
    id: number,
    name: string,
    email: string,
    key: string,
    secret: string
}

export interface ISignUpResponse {
    data: IUserData,
    isOk: boolean,
    message: string
}