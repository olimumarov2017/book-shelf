import { useEffect } from 'react';
import { useAppDispatch } from "@/app/hooks/hook";
import { useForm } from "react-hook-form"
import { yupResolver } from "@hookform/resolvers/yup"
import { setIsLoading, setIsUserAuth, setNotification } from "@/features/counter/bookStore";
import { ISignUp } from "./model/types/signUp";
import { Box, TextField, Typography, Button } from '@mui/material';
import { schema } from './model/schema/schema';
import { useSignUpMutation } from "@/app/services/bookApi";

import { classNames } from '@/shared/lib/classNames';
import cls from './SignUp.module.css';
import { setToken } from '@/shared/lib/localeStorage';

interface ISignUpProps {
    className?: string;
}

export const SignUp = (props: ISignUpProps) => {
    const { className } = props;

    const { register, handleSubmit, formState: { errors } } = useForm({ resolver: yupResolver(schema) })
    const [signUp, { data, isLoading, isSuccess }] = useSignUpMutation()
    const dispatch = useAppDispatch()

    const onSubmit = (formData: ISignUp) => {
        signUp(formData)
    }

    useEffect(() => {
        if(isLoading) dispatch(setIsLoading(true))
        if (isSuccess && data) {
            dispatch(setIsLoading(false))
            dispatch(setIsUserAuth());
            setToken('key', data.data.key)
            dispatch(setNotification({ status: 'success', text: 'You have successfully signed up!' }));
        }
    }, [isSuccess, data]);


    return (
        <div className={classNames(cls.signUp, {}, [className])}>
            <section className={classNames(cls.wrapper, {}, ['container'])}>
                <div className={cls.card} >
                    <Typography
                        className={cls.title}
                        component="h4"
                        variant="h4"
                    >
                        SignUp
                    </Typography>
                    <Box
                        component='form'
                        className={cls.form}
                        onSubmit={handleSubmit(onSubmit)}
                    >
                        <div>
                            <Typography
                                className={cls.inputTitle}
                                component="span"
                            >
                                Name
                            </Typography>
                            <TextField
                                id="name"
                                className={cls.input}
                                error={errors.name?.message ? true : false}
                                helperText={errors.name?.message}
                                type="text"
                                variant="outlined"
                                {...register('name')}
                            />
                        </div>
                        <div>
                            <Typography
                                className={cls.inputTitle}
                                component="span"
                            >
                                E-mail
                            </Typography>
                            <TextField
                                id="email"
                                error={errors.email?.message ? true : false}
                                helperText={errors.email?.message}
                                type="email"
                                variant="outlined"
                                {...register('email')}
                                className={cls.input}

                            />
                        </div>
                        <div>
                            <Typography
                                className={cls.inputTitle}
                                component="span"
                            >
                                Password
                            </Typography>
                            <TextField
                                id="password"
                                error={errors.key?.message ? true : false}
                                helperText={errors.key?.message}
                                type="password"
                                variant="outlined"
                                {...register('key')}
                                className={cls.input}
                            />
                        </div>
                        <div>
                            <Typography
                                className={cls.inputTitle}
                                component="span"
                            >
                                Secret word
                            </Typography>
                            <TextField
                                id="secret"
                                error={errors.secret?.message ? true : false}
                                helperText={errors.secret?.message}
                                type="text"
                                variant="outlined"
                                {...register('secret')}
                                className={cls.input}
                            />
                        </div>
                        <Button
                            variant="contained"
                            type="submit"
                            className={cls.button}
                        >
                            SIGN UP
                        </Button>
                    </Box>
                </div>
            </section>
        </div>
    );
};
