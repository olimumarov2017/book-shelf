import { useEffect } from 'react';
import { Card } from '@/components';
import { useGetBooksQuery } from '@/app/services/bookApi';

import { classNames } from '@/shared/lib/classNames';
import cls from './Books.module.css';
import { useAppDispatch } from '@/app/hooks/hook';
import { setIsLoading } from '@/features/counter/bookStore';


interface IBooksProps {
    className?: string;
}

export const Books = (props: IBooksProps) => {
    const { className } = props;
    const { data, isFetching, isSuccess } = useGetBooksQuery()
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (isFetching) dispatch(setIsLoading(true))
        if (isSuccess) dispatch(setIsLoading(false))
    }, [isFetching, isSuccess]);

    return (
        <div className={classNames(cls.mainPage, {}, [className])}>
            <h1>Books</h1>
            <div className={cls.cards}>
                {data &&
                    data.results.map(book =>
                        <Card data={book} key={book.id} />
                    )
                }
            </div>
        </div>
    );
};
