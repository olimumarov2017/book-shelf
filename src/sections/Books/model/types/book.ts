import { ICard } from "@/components/Card/model/types/card"

// export interface IBooksResponse {
//     data: IBookData[],
//     isOk: boolean,
//     message: string
// }


// export interface IBookData {
//     book: ICard,
//     status: number
// }

export interface IBookResponse {
    count: number,
    next: string,
    previous: null,
    results: IBookData[]
}

export interface IBookData {
    id: number,
    title: string,
    authors:  {
        name: string,
        birth_year: number,
        death_year: number
    }[],
    translators: Array<string>[],
    subjects: Array<string>[],
    bookshelves: Array<string>[],
    languages: Array<string>[],
    copyright: boolean,
    media_type: string,
    formats: {
        "text/html": string,
        "application/epub+zip": string,
        "application/x-mobipocket-ebook": string,
        "application/rdf+xml": string,
        "image/jpeg": string,
        "text/plain": string,
        "application/octet-stream": string
    },
    download_count: number
}