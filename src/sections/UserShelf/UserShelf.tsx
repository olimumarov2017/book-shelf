import { useAppSelector } from '@/app/hooks/hook';
import { Card } from '@/components';

import { classNames } from '@/shared/lib/classNames';
import cls from './UserShelf.module.css';

interface IUserShelfProps {
    className?: string;
}

export const UserShelf = (props: IUserShelfProps) => {
    const { className } = props;
    const { userShelfData } = useAppSelector(state => state.store);

    return (
        <div className={classNames(cls.userShelf, {}, [className])}>
            <h1>User Shelf</h1>
            <div className={cls.cards}>
                {userShelfData?.length ?
                    userShelfData.map(book =>
                        <Card
                            data={book}
                            key={book.id}
                            className={cls.card}
                            action="deleted"
                        />
                    ) :
                    <div>Not added yet</div>
                }
            </div>
        </div>
    );
};
