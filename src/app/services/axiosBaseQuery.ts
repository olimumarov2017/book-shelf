import { $api } from "@/shared/api/api"

type AxiosQueryType<T> =  {
  url: string,
  method: string,
  data?: T,
  params?: any,
  headers?: any
}

export const axiosBaseQuery = <T>() =>
  async ({ url, method, data, params, headers }: AxiosQueryType<T>) => {
    try {
      const result = await $api({
        url: url,
        method,
        data,
        params,
        headers,
      })
      return { data: result.data }
    } catch (axiosError: any) {
      const err = axiosError
      return {
        error: {
          status: err.response?.status,
          data: err.response?.data || err.message,
        },
      }
    }
  }