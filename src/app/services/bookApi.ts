import { createApi } from '@reduxjs/toolkit/query/react'
import { ISignUp, ISignUpResponse } from '@/sections/SignUp/model/types/signUp'
import { IBookResponse } from '@/sections/Books/model/types/book'
import { axiosBaseQuery } from './axiosBaseQuery'

export const bookApi = createApi({
  reducerPath: 'bookApi',
  baseQuery: axiosBaseQuery(),
  endpoints: (build) => ({
    signUp: build.mutation({
      query: (data: ISignUp) => ({ url: 'https://no23.lavina.tech/signup', method: 'post', data: data }),
      transformResponse: (response: ISignUpResponse) => response,
      transformErrorResponse: (response: { status: string | number }) => response.status,
    }),
    getBooks: build.query<IBookResponse, void>({
      query: () => ({ url: 'https://gutendex.com/books', method: 'get' }),
      transformResponse: (response: IBookResponse) => response,
      transformErrorResponse: (response: { status: string | number }) => response.status,
    }),
  }),
})

export const { useSignUpMutation, useGetBooksQuery } = bookApi
