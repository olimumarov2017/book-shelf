import { Route, Routes, BrowserRouter } from "react-router-dom";
import { getToken } from '@/shared/lib/localeStorage'
import { PrivateRouter, PublicRouter } from "./index";
import { useAppSelector } from "../hooks/hook";

export const AppRouter = () => {
    const data = getToken('key')
    const { isUserAuth } = useAppSelector(state => state.store)

    const routes = () => {
        if (data.status === 'authenticated' || isUserAuth) {
            return (
                <Route path="*" element={<PrivateRouter />} />
            )
        } else {
            return (
                <Route path="*" element={<PublicRouter />} />
            )
        }
    }

    return (
        <BrowserRouter>
            <Routes>
                {routes()}
            </Routes>
        </BrowserRouter>
    )
};
