import { Route, Routes } from "react-router-dom";
import { Layout } from "@/components";
import { Books, UserShelf } from "@/sections";

export const PrivateRouter = () => {

    return (
        <Layout>
            <Routes>
                <Route path='/books' element={<Books />} />
                <Route path='/shelf' element={<UserShelf />} />
            </Routes>
        </Layout>
    );
};
