import { Route, Routes } from "react-router-dom";
import { SignUp } from "@/sections";

export const PublicRouter = () => {

    return (
        <Routes>
            <Route path='*' element={<SignUp />} />
        </Routes>
    );
};
