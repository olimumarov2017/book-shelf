import bookStore from '@/features/counter/bookStore'
import { configureStore } from '@reduxjs/toolkit'
import { bookApi } from '../services/bookApi'

export const store = configureStore({
  reducer: {
    store: bookStore,
    [bookApi.reducerPath]: bookApi.reducer,
  },
  middleware: (getDefaultMiddleware) => {
    return getDefaultMiddleware().concat(bookApi.middleware)
  }
})

export type RootState = ReturnType<typeof store.getState>

export type AppDispatch = typeof store.dispatch