import { useState, useEffect } from 'react';
import {
    AppBar, Toolbar, Typography, Box, Avatar, Tooltip, IconButton,
    MenuItem, Menu, List, ListItem
} from '@mui/material';
import { Link } from 'react-router-dom';
import { useAppDispatch } from '@/app/hooks/hook';
import { removeToken } from '@/shared/lib/localeStorage';
import { setIsUserAuth } from '@/features/counter/bookStore';

import { classNames } from '@/shared/lib/classNames';
import cls from './Navbar.module.css';


interface INavbarProps {
    className?: string;
}

export const Navbar = (props: INavbarProps) => {
    const { className } = props;
    const [path, setPath] = useState<string>('')
    const dispatch = useAppDispatch()

    useEffect(() => {
        const pathname = window.location.pathname;
        if (pathname === '') navigateToComponent('/books')
        navigateToComponent(pathname)
    }, []);

    const [anchorElUser, setAnchorElUser] = useState<HTMLElement | null>(null);

    const handleOpenSettingsMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseSettingsMenu = () => {
        setAnchorElUser(null);
    };

    const navigateToComponent = (path: string) => {
        setPath(path)
    }

    const signOut = () => {
        removeToken('key')
        dispatch(setIsUserAuth());
    }

    return (
        <AppBar position="static" className={classNames(cls.navbar, {}, [className])}>
            <Toolbar>
                <Typography
                    variant="h5"
                    component="a"
                    href="/"
                    className={cls.title}
                >
                    Book Shelf
                </Typography>
                <List className={cls.menu}>
                    <ListItem >
                        <Link
                            to="/books"
                            className={classNames('', { [cls.active]: path === '/books' }, [])}
                            onClick={() => navigateToComponent("/books")}
                        >
                            Books
                        </Link>
                    </ListItem>
                    <ListItem >
                        <Link
                            to="/shelf"
                            className={classNames('', { [cls.active]: path === '/shelf' }, [])}
                            onClick={() => navigateToComponent("/shelf")}
                        >
                            Shelf
                        </Link>
                    </ListItem>
                </List>
                <Box className={cls.avatar}>
                    <Tooltip title="Open my_settings">
                        <IconButton onClick={handleOpenSettingsMenu} sx={{ p: 0 }}>
                            <Avatar alt="Remy Sharp" src="/static/images/avatar/2.jpg" />
                        </IconButton>
                    </Tooltip>
                    <Menu
                        sx={{ mt: '55px' }}
                        id="menu-appbar"
                        anchorEl={anchorElUser}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={Boolean(anchorElUser)}
                        onClose={handleCloseSettingsMenu}
                    >
                        <MenuItem onClick={handleCloseSettingsMenu}>
                            <Typography
                                textAlign="center"
                                onClick={signOut}
                            >
                                Logout
                            </Typography>
                        </MenuItem>
                    </Menu>
                </Box>
            </Toolbar>
        </AppBar>
    );
};
