import { Navbar } from './ui';

import cls from './Layout.module.css';
import { classNames } from '@/shared/lib/classNames';


interface ILayoutProps {
    className?: string;
    children: React.ReactNode
}

export const Layout = (props: ILayoutProps) => {
    const { className, children } = props;

    return (
        <div className={classNames(cls.layout, {}, [className])}>
            <Navbar />
            {children}
        </div>
    );
};
