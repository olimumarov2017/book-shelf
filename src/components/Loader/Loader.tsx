import { CircularProgress, Box } from '@mui/material';

import { classNames } from '@/shared/lib/classNames';
import cls from './Loader.module.css';
import { useAppSelector } from '@/app/hooks/hook';


interface ILoaderProps {
    className?: string;
}

export const Loader = (props: ILoaderProps) => {
    const { className } = props;
    const { isLoading } = useAppSelector(state => state.store);

    return (
        <>
            {
                isLoading &&
                <Box className={classNames(cls.loader, {}, [className])}>
                    <CircularProgress />
                </Box>
            }
        </>
    );
};
