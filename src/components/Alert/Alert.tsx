import { useEffect, useState } from 'react';
import { Snackbar, SvgIcon } from '@mui/material';
import { useAppSelector } from '@/app/hooks/hook';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ErrorIcon from '@mui/icons-material/Error';

import { classNames } from '@/shared/lib/classNames';
import cls from './Alert.module.css';


interface IAlertProps {
    className?: string;
}

export const Alert = (props: IAlertProps) => {
    const { className } = props;
    const { notification } = useAppSelector(state => state.store)
    const [open, setOpen] = useState<boolean>(false);

    useEffect(() => {
        notification?.status === 'success' ? setOpen(true) : setOpen(false)
        setTimeout(() => {
            setOpen(false)
        }, 3000)
    }, [notification])

    const action = (
        <SvgIcon className={classNames(cls.icon, { [cls.icon_success]: notification?.status === 'success', [cls.icon_error]: notification?.status === 'error' }, [])}>
            {notification?.status === 'success' ? <CheckCircleIcon /> : <ErrorIcon />}
        </SvgIcon>
    );

    return (
        <div className={classNames(cls.alert, {}, [className])}>
            <Snackbar
                open={open}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                className={classNames(cls.snackbar, { [cls.snackbar_success]: notification?.status === 'success', [cls.snackbar_error]: notification?.status === 'error' }, [className])}
                message={notification?.text}
                action={action}
                autoHideDuration={6000}
            />
        </div>
    );
};
