import { Box, Typography, Button } from '@mui/material';
import { IBookData } from '@/sections/Books/model/types/book';
import { useAppDispatch } from '@/app/hooks/hook';
import { setNotification, setUserShelfData } from '@/features/counter/bookStore';

import { classNames } from '@/shared/lib/classNames';
import cls from './Card.module.css';


interface ICardProps {
    className?: string;
    data: IBookData;
    action: 'added' | 'deleted'
}

export const Card = (props: ICardProps) => {
    const { className, data, action = 'added' } = props;
    const dispatch = useAppDispatch();

    const addToBookShelf = () => {
        dispatch(setUserShelfData({data, action}));
        dispatch(setNotification({ status: 'success', text: `Book's been successfully ${action}!` }));
    }

    return (
        <div className={classNames(cls.card, {}, [className])}>
            <img src={data.formats['image/jpeg']} alt="" />
            <Box className={cls.box}>
                <Typography
                    className={cls.title}
                    component='span'
                >
                    {data.title}
                </Typography>
                <Button
                    variant="contained"
                    className={classNames(cls.button, {[cls.button_danger]: action === 'deleted'}, [])}
                    onClick={addToBookShelf}
                >
                    {action === 'added' ? 'Add' : 'Remove'}
                </Button>
            </Box>
        </div>
    );
};
