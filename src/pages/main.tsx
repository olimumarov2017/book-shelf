import ReactDOM from 'react-dom/client'
import { App } from "./App.tsx";
import { Provider } from "react-redux";
import { store } from '@/app/store/store.ts';
import '@/app/styles/index.css';

const reactRoot = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement,
)

reactRoot.render(
  <Provider store={store}>
    <App />
  </Provider>
);
