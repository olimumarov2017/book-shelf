import { AppRouter } from '@/app/router/AppRouter';
import { Alert, Loader } from '@/components';

export const App = () => {

    return (
        <>
            <Alert />
            <Loader />
            <AppRouter />
        </>
    );
};
